USE warehouse;

INSERT INTO warehouses (address) VALUES ('Street 1');
INSERT INTO warehouses (address) VALUES ('Street 2');
INSERT INTO warehouses (address) VALUES ('Street 3');

INSERT INTO products (name, warehouse_id) VALUES ('red table', 1);
INSERT INTO products (name, warehouse_id) VALUES ('blue table', 1);
INSERT INTO products (name, warehouse_id) VALUES ('green table', 2);
INSERT INTO products (name, warehouse_id) VALUES ('black chair', 3);
INSERT INTO products (name, warehouse_id) VALUES ('red chair', 2);
INSERT INTO products (name, warehouse_id) VALUES ('blue chair', 1);


INSERT INTO suppliers (name) VALUES ('Vasya');
INSERT INTO suppliers (name) VALUES ('Petya');
INSERT INTO suppliers (name) VALUES ('Sasha');
INSERT INTO suppliers (name) VALUES ('Anna');
INSERT INTO suppliers (name) VALUES ('Lena');

