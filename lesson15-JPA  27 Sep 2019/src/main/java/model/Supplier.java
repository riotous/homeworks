package model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NamedQuery(name =  "getAllSuppliers",
        query = "SELECT s FROM Supplier s")
@Entity
@Table(name = "suppliers")
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinTable(name = "products_suppliers",
            joinColumns={@JoinColumn(name = "supplier_id")},
            inverseJoinColumns={@JoinColumn(name = "product_id")})
    private Set<Product> products = new HashSet<Product>();

    public Supplier() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
}
