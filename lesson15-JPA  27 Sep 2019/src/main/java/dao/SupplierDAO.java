package dao;

import model.Supplier;

import java.sql.SQLException;
import java.util.Collection;

public interface SupplierDAO {
    public void addSupplier(Supplier supplier) throws SQLException, Exception;
    public void updateSupplier(Supplier supplier) throws SQLException, Exception;
    public Supplier getSupplierById(Long id) throws SQLException, Exception;
    public Collection<Supplier> getAllSuppliers() throws SQLException, Exception;
    public void deleteSupplier(Supplier supplier) throws SQLException, Exception;
}
