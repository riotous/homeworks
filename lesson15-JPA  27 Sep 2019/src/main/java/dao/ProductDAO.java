package dao;

import model.Product;

import java.sql.SQLException;
import java.util.Collection;

public interface ProductDAO {
    public void addItem(Product product) throws SQLException, Exception;
    public void updateItem(Product product) throws SQLException, Exception;
    public Product getItemById(Long id) throws SQLException, Exception;
    public Collection<Product> getAllItems() throws SQLException, Exception;
    public void deleteItem(Product product) throws SQLException, Exception;
}
