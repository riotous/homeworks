

import daoImpl.ProductDAOImpl;
import daoImpl.WarehouseDAOImpl;
import model.Warehouse;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {
    public static void main(String[] args) throws Exception {
        WarehouseDAOImpl warehouseDAO = new WarehouseDAOImpl();
            warehouseDAO.criteriaQ();
            warehouseDAO.jpqlQ();
            warehouseDAO.namedQ();
            warehouseDAO.nativeQ();
        ProductDAOImpl productDAO = new ProductDAOImpl();
            productDAO.criteriaQ();
            productDAO.jpqlQ();
            productDAO.namedQ();
            productDAO.nativeQ();
    }
}

//        * Необходимо реализовать 2 запроса(
//        join, order by, group by, having ) через
//        все возможные варианты( JPQL, NamedQuery , NativeQuery , CriteriaAPI