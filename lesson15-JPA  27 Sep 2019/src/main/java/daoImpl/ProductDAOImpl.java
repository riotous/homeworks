package daoImpl;

import model.Product;
import model.Warehouse;
import org.hibernate.Session;
import org.hibernate.annotations.NamedQuery;
import util.HibernateUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProductDAOImpl {

    private Session session;

    public ProductDAOImpl() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Collection<Product> getAllProducts() throws SQLException, Exception {
        List<Product> products = new ArrayList<Product>();
        products = session.createCriteria(Product.class).list();
        return products;
    }

    public void addProduct(Product product) throws SQLException, Exception {
        session.beginTransaction();
        session.save(product);
        session.getTransaction().commit();
    }

    public void updateProduct(Product product) throws SQLException, Exception {
        session.beginTransaction();
        session.update(product);
        session.getTransaction().commit();
    }

    public Product getProductById(Long id) throws SQLException, Exception {
        Product product = null;
        product = (Product) session.load(Product.class, id);
        return product;
    }

    public void deleteProduct(Product product) throws SQLException, Exception {
        session.beginTransaction();
        session.delete(product);
        session.getTransaction().commit();
    }

    public void nativeQ() {
        session.createNativeQuery("SELECT * FROM products FULL JOIN warehouses ORDER BY products.id", Product.class).getResultList().forEach(System.out::println);
    }

    public void namedQ() {
        session.createNamedQuery("getAllProducts", Product.class).getResultList().forEach(System.out::println);
    }

    public void jpqlQ() {
        session.createQuery("SELECT p FROM model.Product p").getResultList().forEach(System.out::println);
    }

    @SuppressWarnings("deprecation")
    public void criteriaQ() {
        session.createCriteria(Product.class).setMaxResults(10).list().forEach(System.out::println);
    }
}
