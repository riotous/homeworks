package daoImpl;

import dao.SupplierDAO;
import model.Product;
import model.Supplier;
import util.HibernateUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SupplierDAOImpl implements SupplierDAO {

    private Session session;

    public SupplierDAOImpl() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    @Override
    public Collection<Supplier> getAllSuppliers() throws SQLException, Exception {
        List<Supplier> suppliers = new ArrayList<Supplier>();
        suppliers = session.createCriteria(Supplier.class).list();
        return suppliers;
    }

    @Override
    public void addSupplier(Supplier supplier) throws SQLException, Exception {
        session.beginTransaction();
        session.save(supplier);
        session.getTransaction().commit();
    }

    @Override
    public void updateSupplier(Supplier supplier) throws SQLException, Exception {
        session.beginTransaction();
        session.update(supplier);
        session.getTransaction().commit();
    }
}
