package daoImpl;

import dao.WarehouseDAO;
import model.Warehouse;
import org.hibernate.Session;
import util.HibernateUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class WarehouseDAOImpl implements WarehouseDAO {
    private Session session;

    public WarehouseDAOImpl() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public void addWarehouse(Warehouse warehouse) throws SQLException, Exception {
        session.beginTransaction();
        session.save(warehouse);
        session.getTransaction().commit();
    }

    public void updateWarehouse(Warehouse warehouse) throws SQLException, Exception {
        session.beginTransaction();
        session.update(warehouse);
        session.getTransaction().commit();
    }

    public Warehouse getWarehouseById(Long id) throws SQLException, Exception {
        Warehouse warehouse = null;
        warehouse = (Warehouse) session.load(Warehouse.class, id);
        return warehouse;
    }

    public Collection<Warehouse> getAllWarehouses() throws SQLException, Exception {
        List<Warehouse> warehouses = new ArrayList<Warehouse>();
        warehouses = session.createCriteria(Warehouse.class).list();
        return warehouses;
    }

    public void deleteWarehouse(Warehouse warehouse) throws SQLException, Exception {
        session.beginTransaction();
        session.delete(warehouse);
        session.getTransaction().commit();
        System.out.println("Siccess");
    }

    public void nativeQ(){
        session.createNativeQuery("SELECT * FROM warehouses", Warehouse.class).getResultList().forEach(System.out::println);
    }

    public void namedQ(){
        session.createNamedQuery("getAllWarehouses", Warehouse.class).getResultList().forEach(System.out::println);
    }

    public void jpqlQ(){
        session.createQuery("SELECT p FROM model.Warehouse p").getResultList().forEach(System.out::println);

    }

    public void criteriaQ(){
        session.createCriteria(Warehouse.class).setMaxResults(10).list().forEach(System.out::println);
    }


}
