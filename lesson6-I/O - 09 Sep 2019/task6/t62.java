import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;

public class t62 {

    public static void main(String[] args) throws Exception {
        DataInputStream in = new DataInputStream(new
                BufferedInputStream(new FileInputStream("t6.dat")));

        System.out.println(in.readUTF() + " " + in.readDouble());
    }
}
