import java.io.*;

public class Example implements Serializable {

    First first;
    Second second;

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Example e1 = new Example();
        Example e2 = new Example();
        System.out.println(e1.equals(e2));

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("person.dat"));
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("person.dat"));

        oos.writeObject(e1);
        oos.writeObject(e2);
        Example e3 = (Example)ois.readObject();
        Example e4 = (Example)ois.readObject();

        System.out.println(e3.equals(e4));
    }
}
