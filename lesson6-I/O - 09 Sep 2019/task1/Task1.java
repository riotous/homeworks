import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class Task1 {
    public static void main(String[] args) throws Exception {
        final String FILENAME = "text.txt";
        byte[] byteArray = Files.readAllBytes(Paths.get(FILENAME));
        char[] revertByteArray = new char[byteArray.length];

        for (int i = 0; i < byteArray.length; i++) {
            revertByteArray[i] = (char) byteArray[byteArray.length - 1 - i];
        }
        for (char v : revertByteArray) {
            System.out.print(v);
        }
    }

}


//   1. Считать информацию из текстового файла и вывести ее в
//        консоль, предварительно изменив порядок следования
//        символов на обратный входному.