import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

public class Task2 {
    public static void main(String[] args) throws Exception {

        final String FILENAME = "text.txt";
        List<String> revertedList = new ArrayList<>();
        List<String> list;

        if (Files.exists(Paths.get(FILENAME))) {
            list = Files.readAllLines(Paths.get(FILENAME));
            list.stream().forEach(x -> System.out.println(x));


            for (int i = 0; i < list.size(); i++) {
                revertedList.add(list.get(list.size() - 1 - i));
            }

            for (String v : revertedList) {
                System.out.println(v);
            }
        }
    }
}