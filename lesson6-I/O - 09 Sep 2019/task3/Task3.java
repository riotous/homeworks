import java.io.*;

public class Task3 {
    public static void main(String[] args) throws Exception {

        final String product = "Donut";
        final double price = 20.00;
        final int unit = 12;

        DataOutputStream out = new DataOutputStream(new BufferedOutputStream(
                new FileOutputStream("src\\data.bin")));

        out.writeUTF(product);
        out.writeDouble(price);
        out.writeInt(unit);
        out.flush();

        DataInputStream in = new DataInputStream(new
                BufferedInputStream(new FileInputStream("src\\data.bin")));

        System.out.println(in.readUTF() + " " + in.readDouble() + " " + in.readInt());

    }
}

