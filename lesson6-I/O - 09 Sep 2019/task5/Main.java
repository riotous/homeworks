import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;


public class Main {

    public static void main(String[] args) {
        final String PATH;
        final String FILENAME;

        try(BufferedReader brc = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Please, write down a path : ...");
            //C:\Users\maio0619\Desktop\hw\io4\src\
            PATH = brc.readLine();
            System.out.println("Please, write down a file name : ...");
            //file5.txt
            FILENAME = brc.readLine();
            File file = new File(PATH + FILENAME);
            BufferedReader out = new BufferedReader(new FileReader(file));
            do {
                System.out.println(out.readLine());
                brc.readLine();
            } while (out.ready());
            System.out.println("Its over! Bye!");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Oo-oops... something went wrong... Please, try again and write a new path.");
        }
    }
}
