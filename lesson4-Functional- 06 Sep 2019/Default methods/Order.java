public class Order {

    private OrderStatuses orderStatus;

    public enum OrderStatuses {
        NEW, EXPIRED, CLOSED;
    }

    public Order(OrderStatuses orderStatus) {
        this.orderStatus = orderStatus;
    }
}
