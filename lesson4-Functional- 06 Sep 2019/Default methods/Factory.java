public class Factory implements OrderFactory {

    public Order generateOrder(String status) {
        Order order;
        switch (status) {

            case ("NEW"): {
                order = generateNewOrder();
                break;
            }


            case ("EXPIRED"):{
                order = generateExpiredOrder();
                break;
            }


            case ("CLOSED"):{
                order = generateClosedOrder();
                break;
            }
        }
        return order;
    }
}
