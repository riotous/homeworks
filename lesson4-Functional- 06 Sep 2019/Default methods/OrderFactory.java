public interface OrderFactory {

    default Order generateNewOrder(){
        return new Order(Order.OrderStatuses.NEW);
    }

    default Order generateExpiredOrder(){
        return new Order(Order.OrderStatuses.EXPIRED);
    }

    default Order generateClosedOrder(){
        return new Order(Order.OrderStatuses.CLOSED);
    }

}
