import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Main {

    Optional<Object> optional;

    MyDao myDao;

    private Map<String, Object> myOwnCache = new HashMap<>();

    public void addToCache(String key, Object value) {
        myOwnCache.put(key, value);
    }

    public Object getFromCache(String key) {

        Optional<Object> optional = Optional.of(myOwnCache.get(key));
        optional.orElse(myDao.getFromDB(key));
        return optional.get();

    }

    public void removeFromCache(String key) {
        myOwnCache.remove(key);
    }
}
