import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Main {
    public static void main(String[] args) {

        Queue<Order> q = new LinkedList<>();
        for (int i = 0; i < 3; i++) {
            q.add(new Order());
        }
        Controller(q);
    }

    public static void Controller(Queue<Order> q) {
        ArrayList<Processor> processors = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            processors.add(new Processor());
        }
        while (q.poll() != null)
            for (Processor processor : processors) {
                if (processor.isAvailable) {
                    Thread thread = new Thread(processor);
                    thread.start();
                }
            }
    }

}
