package lesson2;

public class Processor implements Runnable {
    public boolean isAvailable = true;
    Order order;

    @Override
    public void run() {
        isAvailable = false;
        order.id = +5;
        System.out.print("Success");
    }
}
