import java.util.List;

public class MyOwnAPI {

    private int number = 5;

    public void processOrder(List<Order> orders) { //take generic-list
        for (Order i : orders) {
            i.id = +number; // do something
            System.out.println(i.id);
        }
    }

    public int getIdOfFirstElement(List<? extends Order> orders) { //take generic-list
        int result = orders.get(0).id;
        System.out.println(result);
        return result;
    }

    public class Order {
        int id;
    }

}
