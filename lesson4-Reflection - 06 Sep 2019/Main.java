package TestPackage;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Object[] differentObjectArr = new Object[10];
        for (int i = 0; i < 7; i++) {
            differentObjectArr[i] = new DeprecatedClass();
        }
        for (int i = 7; i < 10; i++) {
            differentObjectArr[i] = new String("Something");
        }

        Object[] filtredDifferentObjectArr = new Object[10];
        Reflections reflections = new Reflections("TestPackage");
        ArrayList<Class<? extends NonDeprecatedSuperclass>> suggestions;

        int countObjects = 0;
        for (Object o : differentObjectArr) {
            if (o.getClass().isAnnotationPresent(Deprecated.class)) {
                filtredDifferentObjectArr[countObjects] = o;
                countObjects++;
                //System.out.println(o.getClass().getSuperclass());
                Class clazz = o.getClass().getSuperclass();
                List<Class> listOfClasses = new ArrayList<>();
                if (o.getClass().getSuperclass() != null) {
                    Package p = o.getClass().getPackage();
                    listOfClasses.add(p.getClass());
                    suggestions = reflections.getSubTypesOf(NonDeprecatedSuperclass.class);
                }
            }
        }

        for (Object o: suggestions){
            if (o.getClass().isAnnotationPresent(Deprecated.class)){
                suggestions.remove(o);
            }
        }
        suggestions.trimToSize();
        for (Object o: suggestions) {
            System.out.println("You use deprecated class. You are suggested to use: " + o);
        }
    }
}




