package TestPackage;

import TestPackage.Deprecated;

@Deprecated
public class DeprecatedClass extends NonDeprecatedSuperclass {
    public String name;
    public void doSomething(){
        System.out.println("This is a deprecated class!");
    }
}
