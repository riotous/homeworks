package TestPackage;

import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.PACKAGE, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Deprecated {

}
