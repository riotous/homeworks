import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Custom API for calculations, contain such methods as composition, subtraction, multiplication,
 * division and others.
 * @author Ionov Maksim
 */
public class Calculator {

    private static final Logger logger = Logger.getLogger(Calculator.class);

    public int composition(int i, int y) {
        logger.info("Composition is invoked");
        return i + y;
    }

    public int subtraction(int i, int y) {
        if (i >= y) {
            logger.info("Substraction is invoked");
            return i - y;
        } else {
            logger.error("Error was occurred during subtraction. Check the arguments.");
            throw new ArithmeticException();
        }
    }

    public int multiplication(int i, int y) {
        logger.info("Multiplication is invoked");
        return i * y;
    }

    public int division(int i, int y) {
        if (i % y == 0) {
            return i / y;
        } else if (y == 0) {
            logger.error("Division to zero is forbidden!");
            throw new IllegalArgumentException("/ to zero");
        } else {
            logger.warn("You are about to divide with the remainder, and, lose the remainder from the division!");
            return (int) (i / y);
        }
    }

    public int sqrt(int i) {
        if (i >= 0) {
            logger.warn("Squarerooting is invoked. Attention, you will lose remainder");
            return (int) Math.sqrt(i);
        } else {
            logger.error("Argument is unappropriate!");
            throw new IllegalArgumentException("Argument can`t be negative!");
        }
    }

    public boolean isPrime(int y) {
        if (y <= 1) {
            logger.error("Argument should be bigger then " + y + "!");
            throw new IllegalArgumentException("Argument should be bigger then " + y + "!");
        } else {
            logger.info("isPrime check is invoked");
            List<Integer> listOfDivisors = IntStream.range(2, y / 2 + 1).filter(num -> y % num == 0).boxed().collect(Collectors.toList());
            return listOfDivisors.isEmpty();
        }
    }

    public int fibonacci(int n) {
        if (n >= 0) {
            if (n == 0) {
                return 0;
            } else if (n == 1) {
                return 1;
            } else {
                return fibonacci(n - 1) + fibonacci(n - 2);
            }
        } else {
            logger.warn("You are about to use negative digits");
            throw new ArithmeticException("");
        }
    }

    public int summarizeNumberRow(int[] row) {
        if (row.length != 0) {
            logger.info("Summarization of array is invoked");
            return Arrays.stream(row).sum();
        } else {
            logger.error("You have no array!");
            throw new IllegalArgumentException("You have no array!");
        }
    }
}

