import org.apache.log4j.Logger;

public class NumberRowSupplier {

    private static final Logger logger = Logger.getLogger(NumberRowSupplier.class);

    final int[] finalMethod() {
        logger.info("Final Method is started.");
        return new int[]{1, 2, 3};
    }

    static int[] staticMethod() {
        logger.debug("Static method is started.");
        return new int[]{4, 5, 6};
    }

    private int[] privateMethod() {
        logger.trace("Private method is started");
        return new int[]{7, 8, 9};
    }

    public int[] getRowFromPrivateMethod() {
        logger.trace("getRowFromPrivateMethod is started");
        return privateMethod();
    }
}
