
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CalculatorTestNegative {
    static Calculator calculator;

    public CalculatorTestNegative() {
    }

    @BeforeClass
    public static void init() {
        calculator = new Calculator();
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    @Test
    public void DivideToZero() {
        expectedException.expect(ArithmeticException.class);
        expectedException.expectMessage("/ by zero");
        calculator.division(234, 0);
    }

    @Test
    public void sqrt() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Argument");
        calculator.sqrt(-9);
    }

    @Test
    public void IsPrime_lessOne() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Argument should be bigger then -3!");
        calculator.isPrime(-3);
    }

    @Test
    public void fibonacci() {
        expectedException.expect(ArithmeticException.class);
        calculator.fibonacci(-7);
    }
}
