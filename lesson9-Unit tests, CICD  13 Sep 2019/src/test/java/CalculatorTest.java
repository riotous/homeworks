import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(NumberRowSupplier.class)
class CalculatorTest {
    static Calculator calculator;

    public CalculatorTest() {
    }

    @BeforeClass
    public static void init() {
        calculator = new Calculator();
    }

    @Test
    public void composition() {
        int actual = calculator.composition(5, 6);
        Assert.assertEquals(11, actual);
        Assert.assertEquals(20, calculator.composition(14, 6));
        Assert.assertEquals(-35, calculator.composition(5, -40));
    }

    @Test
    public void subtraction() {
        int actual = calculator.subtraction(10, 6);
        Assert.assertEquals(4, actual);
    }

    @Test
    public void multiplication() {
        int actual = calculator.multiplication(10, 6);
        Assert.assertEquals(60, actual);



    }

    @Test
    public void division() {
        int actual = calculator.division(10, 2);
        Assert.assertEquals(5, actual);
        Assert.assertEquals(2, calculator.division(7, 3));
    }

    @Test
    public void sqrt() {
        int actual = calculator.sqrt(10);
        Assert.assertEquals(3, actual);
    }

    @Test(timeout = 567)
    public void isPrime() {
        Assert.assertTrue(calculator.isPrime(17));
    }

    @Test
    public void fibonacci() {
        int actual = calculator.fibonacci(7);
        Assert.assertEquals(13, actual);
    }

    @Test
    public void RowSummarizing() {
        NumberRowSupplier numberRowSupplier = new NumberRowSupplier();
        Assert.assertEquals(15, calculator.summarizeNumberRow(NumberRowSupplier.staticMethod()));
        Assert.assertEquals(6, calculator.summarizeNumberRow(numberRowSupplier.finalMethod()));
    }

    @Test
    public void MocksNumberRowSupplier() throws Exception {
        NumberRowSupplier mock = PowerMockito.mock(NumberRowSupplier.class);
        int[] finalMock = new int[]{4, 2, 7};
        PowerMockito.when(mock.finalMethod()).thenReturn(finalMock);
        Assert.assertEquals(13, calculator.summarizeNumberRow(mock.finalMethod()));

        PowerMockito.mockStatic(NumberRowSupplier.class);
        int[] staticMock = new int[]{4, 2, 1};
        PowerMockito.when(NumberRowSupplier.staticMethod()).thenReturn(staticMock);
        Assert.assertEquals(7, calculator.summarizeNumberRow(NumberRowSupplier.staticMethod()));

        int[] spyArray = new int[]{17, 3, 5};
        NumberRowSupplier privateSpy = PowerMockito.spy(new NumberRowSupplier());
        PowerMockito.when(privateSpy, MemberMatcher.method(NumberRowSupplier.class, "privateMethod")).withNoArguments().thenReturn(spyArray);
        Assert.assertEquals(25, calculator.summarizeNumberRow(privateSpy.getRowFromPrivateMethod()));

        NumberRowSupplier mockitoMock = Mockito.mock(NumberRowSupplier.class);
        int[] mockitoPublicValues = new int[]{8, 2, 5};
        Mockito.when(mockitoMock.getRowFromPrivateMethod()).thenReturn(mockitoPublicValues);
        Assert.assertEquals(15, calculator.summarizeNumberRow(mockitoMock.getRowFromPrivateMethod()));
    }
}

