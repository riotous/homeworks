import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

public class Parser {

    public static void main(String[] args) throws FileNotFoundException {
        JsonReader jr = Json.createReader(new FileInputStream("example.json"));
        JsonObject jo = jr.readObject();
        jo.entrySet().stream().forEach(x -> System.out.println(x));
        JsonArray ja = jo.getJsonArray("data");
        String result =
                "Node with name " + jo.getJsonObject("Brand").toString() + " has\n type " + ja.getJsonObject(0).getValueType() + " and value: "
                + ja.getJsonObject(0).getValueType();
    }
}

