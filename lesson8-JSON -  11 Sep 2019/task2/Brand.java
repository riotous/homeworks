public class Brand {

    String brandName;

    public class Branch {

        String identification;
        String sequenceNumber;
        String name;
        String type;
        String[] customerSegment;
        String[] accessibility;
        OtherServiceAndFacility[] otherServiceAndFacility;
        Availability availability;
        ContactInfo[] contactInfo;
        PostalAddress postalAddress;
    }
}
