import com.google.gson.Gson;

public class Deserialisator {

    public Object deserialise(String json, Class<?> clazz){
        Gson gson = new Gson();
        return gson.fromJson(json, clazz);
    }

    public String serialise(Object object){
        Gson gson = new Gson();
        return gson.toJson(object);
    }

}


