import java.util.Objects;

public class SerializationExample {
    public String string = "Something";
    public int someInt = 8;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SerializationExample that = (SerializationExample) o;
        return someInt == that.someInt &&
                Objects.equals(string, that.string);
    }

    @Override
    public int hashCode() {
        return Objects.hash(string, someInt);
    }

    @Override
    public String toString() {
        return "SerializationExample{" +
                "string='" + string + '\'' +
                ", someInt=" + someInt +
                '}';
    }
}
