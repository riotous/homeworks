import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class DeserialisatorTest {

    static Deserialisator deserialisator;
    static SerializationExample se;

    @BeforeClass
    public static void initObject() {
        deserialisator = new Deserialisator();
        se = new SerializationExample();
    }

    @Test
    public void deserialise() {
        String json = "{\"string\":\"Something\",\"someInt\":8}";
        Class<?> clazz = SerializationExample.class;
        SerializationExample obj = (SerializationExample) deserialisator.deserialise(json, SerializationExample.class);
        Assert.assertEquals(se, obj);
    }

    @Test
    public void serialise() {
        String expected = "{\"string\":\"Something\",\"someInt\":8}";
        String actual = deserialisator.serialise(se);
        Assert.assertEquals(expected, actual);
    }
}