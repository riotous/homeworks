package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import domain.Order;
import service.OrderServiceImpl;

@RestController
public class OrderController {

    OrderServiceImpl orderService;

    @Autowired
    public OrderController(OrderServiceImpl orderService) {
        this.orderService = orderService;
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.CREATED)
    public Order greeting(@RequestParam(value="title", defaultValue="Unknown") String title, @RequestParam(value="id") int id) {
        Order order = new Order();
        order.setId(id);
        order.setTitle(title);
        orderService.getAll().add(order);
        return order;
    }

    @RequestMapping("/all")
    public List<Order> getListOfOrders() {
        return orderService.getAll();
    }

    @GetMapping("/get")
    public Order getOrderById(@RequestParam(value="id") int id) {
        return orderService.getById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderById(@PathVariable int id) {
        orderService.getAll().remove(id);
    }


    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateOrder(@PathVariable int id,
                            @RequestBody Order order) {
        orderService.getAll().add(id, order);
        orderService.getAll().remove(id+1);
    }

}
