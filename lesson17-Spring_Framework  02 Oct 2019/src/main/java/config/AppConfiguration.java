package config;
import controller.OrderController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


import domain.Order;
import repository.OrderRepositoryImpl;
import service.OrderServiceImpl;


@Configuration
public class AppConfiguration {

    @Bean
    public OrderRepositoryImpl getDAO(){
        return new OrderRepositoryImpl();
    }

    @Bean
    public OrderServiceImpl getService(){
        return new OrderServiceImpl(getDAO());
    }

    @Bean
    public OrderController g(){
        return new OrderController(getService());
    }

}
