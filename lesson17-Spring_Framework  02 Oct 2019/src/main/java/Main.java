import config.AppConfiguration;
import controller.OrderController;
import domain.Order;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import service.OrderServiceImpl;

public class Main {

    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfiguration.class);

        OrderController orderServiceImpls = (OrderController) ctx.getBean(OrderController.class);
        orderServiceImpls.getListOfOrders().stream().forEach(System.out::println);

    }
}
