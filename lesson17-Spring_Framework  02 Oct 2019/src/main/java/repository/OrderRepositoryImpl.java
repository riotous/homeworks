package repository;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.Order;
import org.springframework.stereotype.Component;

@Component
public class OrderRepositoryImpl implements OrderRepository {


    private static Connection getH2Connection() throws SQLException {
        return DriverManager.getConnection("jdbc:h2:mem:");
    }

    private List<Order> orders = new ArrayList<Order>();

    public OrderRepositoryImpl() {
        Order order = new Order();
        order.setId(1);
        order.setPrice(234d);
        order.setTitle("Pizza peperoni");
        Order order2 = new Order();
        order2.setId(2);
        order2.setPrice(123d);
        order2.setTitle("Sushi Philadelfia");
        orders.add(order);
        orders.add(order2);
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void save(Order order) {
        orders.add(order);
    }

    public void delete(Order order) {
        orders.remove(order);
    }

    public List<Order> getAll() {
        return orders;
    }

    public Order getById(Integer id) {
        return orders.get(id);
    }

}
