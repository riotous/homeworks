package repository;
import java.util.List;

import domain.Order;

public interface OrderRepository {
    void save(Order order);

    //void delete(int);

    List<Order> getAll();

    Order getById(Integer id);
}
