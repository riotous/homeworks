package service;
import java.util.List;

import domain.Order;
import org.springframework.beans.factory.annotation.Autowired;
import repository.OrderRepository;
import repository.OrderRepositoryImpl;

public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepositoryImpl orderRepository;

    public OrderServiceImpl(OrderRepositoryImpl orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<Order> getAllOrders(){
        return orderRepository.getOrders();
    }

    public Order getById(int id){
        return  orderRepository.getOrders().get(id);
    }

    public void save(Order order) {
        if(order!=null) {
            List<Order> orders = orderRepository.getAll();
            if(!orders.isEmpty()) {
                Order lastOrder = orders.get(orders.size() - 1);
                order.setId(lastOrder.getId()+1);
                orderRepository.save(order);
            }
        }
    }

    public void delete(int id) {
        if(orderRepository.getOrders().get(id)!=null) {
            orderRepository.getOrders().remove(id);
        }
    }

    public List<Order> getAll() {
        return orderRepository.getAll();
    }

    public Order getById(Integer id) {
        if(id!=null) {
            return orderRepository.getById(id);
        }
        return null;
    }
}
