package service;
import java.util.List;

import domain.Order;

public interface OrderService {
    void save(Order order);

    void delete(int id);

    List<Order> getAll();

    Order getById(Integer id);
}
