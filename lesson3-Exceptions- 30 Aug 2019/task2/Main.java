public class Main {
    public static void main(String[] args) {
        try(MyOwnAutoCloseable moac = new MyOwnAutoCloseable()){
            moac.doSomething();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
