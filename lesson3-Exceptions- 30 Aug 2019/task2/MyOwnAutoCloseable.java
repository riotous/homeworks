public class MyOwnAutoCloseable implements AutoCloseable {

    public void doSomething(){
        System.out.println("We are doing something");
    }

    @Override
    public void close() throws Exception {
        System.out.println("Closed!");
    }
}
