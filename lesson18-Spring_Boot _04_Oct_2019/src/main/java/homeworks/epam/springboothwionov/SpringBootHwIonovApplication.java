package homeworks.epam.springboothwionov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootHwIonovApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootHwIonovApplication.class, args);
	}

}
