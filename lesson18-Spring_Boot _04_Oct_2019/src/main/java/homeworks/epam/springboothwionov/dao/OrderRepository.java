package homeworks.epam.springboothwionov.dao;

import homeworks.epam.springboothwionov.domain.Order;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

public class OrderRepository {
    //for tests and developments
    private List<Order> orders = new ArrayList<Order>();
    //JPA for persistence of objects
    @PersistenceContext
    private EntityManager entityManager;



    public OrderRepository() {
        Order order = new Order();
        order.setId(1);
        order.setTitle("Pizza peperoni");
        Order order2 = new Order();
        order2.setId(2);
        order2.setTitle("Sushi Philadelfia");
        orders.add(order);
        orders.add(order2);
    }


    //Underlined 5 methods were created for testing and development
    public List<Order> getOrders() {
        return orders;
    }
    public void save(Order order) {
        orders.add(order);
    }
    public void delete(Order order) {
        orders.remove(order);
    }
    public List<Order> getAll() {
        return orders;
    }
    public Order getById(Integer id) {
        return orders.get(id);
    }

    //Underlined 5 methods use JPA with either H2 or PostgreSQL

    public List<String> getOrdersJPA() {
        return entityManager.createNativeQuery("SELECT * FROM `orders`").getResultList();
    }


    public void createOrderJPA(Order order) {
        entityManager.persist(order);
    }


    public Order findJPA(int id) {
        return entityManager.find(Order.class, id);
    }


    public void updateJPA(Order order) {
        entityManager.merge(order);
    }


    public void deleteJPA(int id) {
        entityManager.remove(findJPA(id));
    }



}
