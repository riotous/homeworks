package homeworks.epam.springboothwionov.config;

import homeworks.epam.springboothwionov.dao.OrderRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

//    @Bean
//    public Order getOrder(){
//        return new Order();
//    }


    @Bean
    public OrderRepository getDAO() {
        return new OrderRepository();
    }


}
