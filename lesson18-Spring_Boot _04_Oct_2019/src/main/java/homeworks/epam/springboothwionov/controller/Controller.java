package homeworks.epam.springboothwionov.controller;

import homeworks.epam.springboothwionov.domain.Order;
import homeworks.epam.springboothwionov.dao.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class Controller {

    OrderRepository orderService;

    @Autowired
    public Controller(OrderRepository orderService) {
        this.orderService = orderService;
    }


    @RequestMapping(value = "/create", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.CREATED)
    public Order greeting(@RequestParam(value="title", defaultValue="Unknown") String title, @RequestParam(value="id") int id) {
        Order order = new Order();
        order.setId(id);
        order.setTitle(title);
        orderService.getOrders().add(order);
        return order;
    }

    @RequestMapping("/all")
    public List<Order> getListOfOrders() {
        return orderService.getAll();
    }

    @GetMapping("/get")
    public Order getOrderById(@RequestParam(value="id") int id) {
        return orderService.getById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderById(@PathVariable int id) {
        orderService.getOrders().remove(id);
    }


    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateOrder(@PathVariable int id,
                            @RequestBody Order order) {
        orderService.getOrders().add(id, order);
        orderService.getOrders().remove(id+1);
    }
}
