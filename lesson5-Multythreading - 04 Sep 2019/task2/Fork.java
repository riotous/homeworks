class Fork {

    private boolean available = true;

    public boolean isAvailable() {
        return available;
    }

    public synchronized void takeAFork(){
        available = false;
    }

    public synchronized void putAFork(){
        available = true;
    }
}
