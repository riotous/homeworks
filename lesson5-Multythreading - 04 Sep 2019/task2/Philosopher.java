import java.lang.Runnable;
import java.util.Random;

class Philosopher implements Runnable {

    private int number;

    private synchronized Fork leftFork;
    private synchronized Fork rightFork;

    private STATES state;

    private enum STATES {
        THINKING,
        EATING;
    }

    public Philosopher(int number, Fork leftFork, Fork rightFork) {
        this.number = number;
        this.leftFork = leftFork;
        this.rightFork = rightFork;
        state = STATES.THINKING;
    }


    @Override
    public void run() {
        System.out.println("Philosopher № " + number + " is thinking.");
        while (true) {
            if (rightFork.isAvailable()) {
                rightFork.takeAFork();
                if (leftFork.isAvailable()) {
                    leftFork.takeAFork();
                    state = STATES.EATING;
                    System.out.println("Philosopher № " + number + " is eating.");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Philosopher № " + number + " is full.");
                    state = STATES.THINKING;
                    leftFork.putAFork();
                    rightFork.putAFork();
                    try {
                        Thread.sleep((int) Math.random() * 100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    rightFork.putAFork();
                    try {
                        Thread.sleep((int) Math.random() * 100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
