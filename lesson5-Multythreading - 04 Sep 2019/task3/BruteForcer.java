import org.apache.commons.codec.digest.DigestUtils;

public class BruteForcer implements Runnable {

    public void run() {
        int countInvokes = 0;
        String pass;
        do {
            pass = PassGenerator.generatePass();
            countInvokes++;
        } while (DigestUtils.md5Hex(pass) != "DDC4035FF6451F85BB796879E9E5EA49");
        System.out.println("The password is: " + pass + "\nInvokations: " + countInvokes);
    }
}