public class BankUser implements Runnable{
    Bank bank;
    String userName;

    public BankUser(Bank bank, String userName) {
        this.bank = bank;
        this.userName = userName;
    }

    @Override
    public void run() {
        while (bank.hasMoney(50)){
            try {
                bank.transferMoney(50);
                Thread.sleep(300);
                System.out.println("user " + userName+ " withdrawed 50 RUB");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("Sorry, bank lost all money!");
    }



}
