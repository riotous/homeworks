public class Bank {

    private int moneyAmount;

    public Bank(int moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public boolean hasMoney(int amount){
        if (moneyAmount>=amount){
            return true;
        } else {
            return false;
        }

    }

    public synchronized void transferMoney(int amount) throws Exception{
        if (this.hasMoney(amount)){
            moneyAmount = moneyAmount - amount;
        } else {
            throw new RuntimeException();
        }
    }

    public static void main(String[] args) {
        Bank tinkoff = new Bank(3000);

        Thread t1 = new Thread(new BankUser(tinkoff, "Oleg"));
        Thread t2 = new Thread(new BankUser(tinkoff,"German Greff" ));
        Thread t3 = new Thread(new BankUser(tinkoff, "Vladimir"));

        t1.start();
        t2.start();
        t3.start();

    }

}



//        Написать класс
//        BankUser , который в отдельном потоке будет снимать деньги с банка (банк передается
//        при создании объекта BankUser ). Снятие производится до тех пор, пока на счету банка есть User
//        сначала делает запрос на определенную сумму и если hasMoney возвращает true, вызывает transferMoney
//
//
//        Запустить несколько объектов
//        BankUser на выполнение в разных потоках с одним объектом типа Bank
//        Проанализировать, почему может выбрасываться ошибка при
//        transferMoney
//        Избавиться от ошибки, изменив ранее созданный код:
//        •
//        с использованием стандартных средств синхронизации
//        •
//        с использованием пакета java.util.concurrent что угодно на ваш выбор)

