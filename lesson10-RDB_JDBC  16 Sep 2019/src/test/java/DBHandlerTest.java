import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;

public class DBHandlerTest {

    static DBHandler dbHandler;

    @BeforeClass
    public static void initDB() throws SQLException, ClassNotFoundException {
        dbHandler.estimateConnectionToDB();
        dbHandler.createDB();
        dbHandler.createThreeTables();
        dbHandler.fillInThreeTables();
    }

    @AfterClass
    public static void dropDBandCloseConnection() {
        dbHandler.dropDBandCloseConnection();
    }


    @Test
    public void callableStatement() {
        dbHandler.callableStatement();
    }

    @Test
    public void preparedStatement() {
        dbHandler.preparedStatement();
    }

    @Test
    public void updateWithTrigger() {
        dbHandler.trigger();
    }

    @Test
    public void aggregateFunction(){
        dbHandler.aggregateFunction();
    }
}