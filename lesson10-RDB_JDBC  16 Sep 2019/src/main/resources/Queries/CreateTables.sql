DROP TABLE IF EXISTS faculty;
DROP TABLE IF EXISTS students;
DROP TABLE IF EXISTS university;

CREATE TABLE students (
    id SERIAL NOT NULL,
    name VARCHAR(255),
    sex VARCHAR(255),
    degree VARCHAR(255),
    faculty VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE faculty (
    id SERIAL NOT NULL,
    faculty_name VARCHAR(255),
    university VARCHAR(255),
    PRIMARY KEY (id),
    FOREIGN KEY (university) references university (name)
);

CREATE TABLE university (
    id SERIAL NOT NULL,
    title VARCHAR(255),
    PRIMARY KEY (id)
);
