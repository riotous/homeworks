INSERT INTO students (name, sex, degree, faculty)
VALUES ('Vasiliy', 'male', 'bachelor', 'machinebuilding');
INSERT INTO students (title, genre, members_number)
VALUES ('Sveta', 'female', 'bachelor', 'chemistry');
INSERT INTO students (title, genre, members_number)
VALUES ('Djeeraj', 'male', 'bachelor', 'oil');
INSERT INTO students (title, genre, members_number)
VALUES ('Jackson', 'male', 'bachelor', 'software development');
INSERT INTO students (title, genre, members_number)
VALUES ('John', 'male', 'master', 'engineer');

INSERT INTO faculty (faculty_name, university)
VALUES ('MEH', 'MGU');
INSERT INTO faculty (faculty_name, university)
VALUES ('AFG', 'SamGTU');
INSERT INTO faculty (faculty_name, university)
VALUES ('AHGAS', 'Aerospace University');
INSERT INTO faculty (faculty_name, university)
VALUES ('ASGH', 'MIT');
INSERT INTO faculty (faculty_name, university)
VALUES ('RaSr', 'Cambridge University');

INSERT INTO university (title)
VALUES ('MGU');
INSERT INTO university (title)
VALUES ('SamGTU');
INSERT INTO university (title)
VALUES ('Aerospace University');
INSERT INTO university (title)
VALUES ('MIT');
INSERT INTO university (title)
VALUES ('Cambridge University');
