import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class DBHandler extends Configs {

    Connection connection;

    public void estimateConnectionToDB() {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/", dbUser, dbPass);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void createDB() {
        try {
            connection.createStatement().executeUpdate("DROP DATABASE IF EXIST testDB;");
            connection.createStatement().executeUpdate("CREATE DATABASE testDB");
            connection.createStatement().executeUpdate("create schema if not exists testschema");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void createThreeTables() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("C:\\Users\\maio0619\\Desktop\\homeworks"
                    + "-master\\homeworks-master\\lesson10-RDB_JDBC  16 Sep "
                    + "2019\\src\\main\\resources\\Queries\\CreateTables.sql")));
            StringBuilder builder = new StringBuilder();
            String query = " ";
            while (true) {
                if (!((query = br.readLine()) != null)) break;
                builder.append(query).append(" ");
            }
            System.out.println(builder.toString());
            connection.createStatement().executeUpdate(builder.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void fillInThreeTables() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("C:\\Users\\maio0619\\Desktop\\homeworks"
                    + "-master\\homeworks-master\\lesson10-RDB_JDBC  16 Sep "
                    + "2019\\src\\main\\resources\\Queries\\FillInTheTable.sql")));
            String query;
            StringBuilder builder = new StringBuilder();
            while ((query = br.readLine()) != null) {
                builder.append(query).append(" ");
            }
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void preparedStatement() {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM ? WHERE students.id < ? "
                    + "OUTER JOIN warhouses SORT BY ?;");
            preparedStatement.setString(1, "students");
            preparedStatement.setString(2, "COUNT(faculty.id)");
            preparedStatement.setString(3, "students.id");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                System.out.printf("%d. %s \n", id, name);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void callableStatement() {
        try {
            CallableStatement callableStatement = connection.prepareCall(" { call myfunc(?,?) } ");
            callableStatement.setString(1, "Dima");
            callableStatement.setString(2, "Alex");
            ResultSet result3 = callableStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void trigger() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("Queries/Trigger.sql")));
            String query;
            StringBuilder builder = new StringBuilder();
            while ((query = br.readLine()) != null) {
                builder.append(query).append(" ");
            }
            Statement statement1 = connection.createStatement();
            statement1.executeUpdate(builder.toString());
            Statement statement2 = connection.createStatement();
            statement2.executeUpdate("INSERT INTO students (name, sex, degree, faculty)\n" +
                    "VALUES ('Helga', 'female', 'bachelor', 'LAW');");
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    public void aggregateFunction() {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate("SELECT MAX(Capacity) FROM `faculty`;");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Finally closes connection.
     */
    public void dropDBandCloseConnection() {
        try {
            //Statement statement = connection.createStatement();
            // statement.executeUpdate("DROP DATABASE warehouse;");
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}